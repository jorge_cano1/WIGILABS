package com.wigilabs.authentication.client;

import com.wigilabs.authentication.constants.Constants;
import com.wigilabs.authentication.model.Authentication;
import com.wigilabs.authentication.model.Request;
import com.wigilabs.authentication.model.Response;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class AuthenticationClient {

    public ResponseEntity<Response> authenticate() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set(Constants.HEADER_KEY, Constants.HEADER_VALUE);
        Request request = new Request();

        Authentication authentication = new Authentication(Constants.USER_NAME, Constants.PASSWORD);
        request.setData(authentication);

        HttpEntity<Request> entity = new HttpEntity<>(request, headers);
        return restTemplate.exchange(Constants.URL, HttpMethod.POST, entity, Response.class);
    }
}
