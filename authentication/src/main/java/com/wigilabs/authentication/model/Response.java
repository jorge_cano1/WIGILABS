package com.wigilabs.authentication.model;

public class Response {
    public DataUser response;
    public int error;

    public DataUser getResponse() {
        return response;
    }

    public void setResponse(DataUser response) {
        this.response = response;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }
}
