package com.wigilabs.authentication.model;

public class DataUser {
    public User usuario;

    public User getUsuario() {
        return usuario;
    }

    public void setUsuario(User usuario) {
        this.usuario = usuario;
    }
}
