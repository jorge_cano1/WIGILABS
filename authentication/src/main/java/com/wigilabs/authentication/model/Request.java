package com.wigilabs.authentication.model;

public class Request {
    public Authentication data;

    public Authentication getData() {
        return data;
    }

    public void setData(Authentication data) {
        this.data = data;
    }
}
