package com.wigilabs.authentication;

import com.wigilabs.authentication.client.AuthenticationClient;
import com.wigilabs.authentication.model.Response;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootApplication
public class AuthenticationApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(AuthenticationApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		AuthenticationClient authenticationClient = new AuthenticationClient();



		try {
			ResponseEntity<Response> response = authenticationClient.authenticate();
			if(response.getStatusCode()==HttpStatus.OK){
				if (response.getBody().getError() == 0) {
					System.out.println("Nombre: " + response.getBody().getResponse().getUsuario().getNombre());
					System.out.println("Apellido: " + response.getBody().getResponse().getUsuario().getApellido());
					System.out.println("Correo: " + response.getBody().getResponse().getUsuario().getUserProfileID());
					System.out.println("Numero de documento:  " + response.getBody().getResponse().getUsuario().getDocumentNumber());
				}
			}
		}catch (Exception e){
			System.out.println("Error al loguearse "+ e.getLocalizedMessage());
		}

	}
}
