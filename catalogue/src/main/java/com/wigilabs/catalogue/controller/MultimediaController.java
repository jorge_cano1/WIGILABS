package com.wigilabs.catalogue.controller;

import com.wigilabs.catalogue.model.Multimedia;
import com.wigilabs.catalogue.service.IMultimediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@RestController
public class MultimediaController{

    @Autowired
    IMultimediaService iMultimediaService;

    @GetMapping("/finalMultimedia")
    public List<Multimedia> findAll(){
            return iMultimediaService.findAll();

    }

    @PostMapping("/insertMultimedia")
    public Multimedia insertMultimedia(@RequestBody Multimedia multimedia){
       return iMultimediaService.saveMultimedia(multimedia);
    }
    @PutMapping("/updateMultimedia")
    public Multimedia updateMultimedia(@RequestBody Multimedia multimedia){
        if (multimedia.getId()!=null){
            return iMultimediaService.saveMultimedia(multimedia);
        }
        else return null;



    }
    @DeleteMapping("/deleteMultimedia/{id}")
    public String deleteMultimedia(@PathVariable("id") long id) {

        return iMultimediaService.delete(id);


    }




}
