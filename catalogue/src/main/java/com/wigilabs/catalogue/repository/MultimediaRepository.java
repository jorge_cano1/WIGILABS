package com.wigilabs.catalogue.repository;

import com.wigilabs.catalogue.model.Multimedia;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MultimediaRepository  extends CrudRepository<Multimedia, Long> {

}

