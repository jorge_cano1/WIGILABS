package com.wigilabs.catalogue.service;

import com.wigilabs.catalogue.repository.MultimediaRepository;
import com.wigilabs.catalogue.model.Multimedia;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MultimediaService implements IMultimediaService {

    @Autowired
    private MultimediaRepository multimediaRepository;

    @Override
    public List<Multimedia> findAll() {
        return (List<Multimedia>) multimediaRepository.findAll();
    }

    @Override
    public Multimedia saveMultimedia(Multimedia multimedia) {
        multimediaRepository.save(multimedia);
        return multimedia;
    }

    @Override
    public String delete(Long id) {
        try {
            Multimedia multimedia = multimediaRepository.findById(id).get();
            multimediaRepository.delete(multimedia);
            return "Eliminacion exitosa";
        } catch (Exception e) {
            return "Error al eliminar";
        }
    }

}
