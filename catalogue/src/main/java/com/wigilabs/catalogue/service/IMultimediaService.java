package com.wigilabs.catalogue.service;

import com.wigilabs.catalogue.model.Multimedia;

import java.util.List;


public interface IMultimediaService {
    List<Multimedia> findAll();
    Multimedia saveMultimedia(Multimedia multimedia);
    String delete(Long id);
}
